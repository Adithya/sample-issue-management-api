package com.rest.dao;

import java.util.List;
import com.rest.model.Issue;

public class IssueDao extends BaseDao {

	public List<Issue> getAll(Class clazz) {
		return (List) super.get(Issue.class);
	}

	public Issue getById(int id){
		return (Issue) super.getById(id, Issue.class);
	}
	
	public Issue create(Issue issue){
		return (Issue) super.save(issue);
	}
	
	public Issue update(Issue issue){
		return(Issue) super.update(issue);
	}
	
	public void delete(int id){
		super.delete(id, Issue.class);
	}
}
