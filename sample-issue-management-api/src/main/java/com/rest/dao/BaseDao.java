package com.rest.dao;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

public class BaseDao {

	private static Configuration con = new Configuration().configure();

	public static List<Object> get(Class clazz) {
		SessionFactory sf = con.buildSessionFactory();
		Session session = sf.openSession();
		Transaction t = session.beginTransaction();

		// code to get object by id
		Criteria cr = session.createCriteria(clazz);
		List list = new ArrayList<Object>();
		list = cr.list();
		t.commit();
		session.close();
		return list;

	}

	public static Object getById(int id, Class clazz) {
		SessionFactory sf = con.buildSessionFactory();
		Session session = sf.openSession();
		Transaction t = session.beginTransaction();

		// code to get object by id
		Criteria cr = session.createCriteria(clazz);
		Criterion cn = Restrictions.eq("id", id);
		cr.add(cn);
		Object objectSaved;
		try {
			List l = cr.list();
			objectSaved = l.get(0);
		} catch (Exception e) {
			objectSaved = null;
		}
		t.commit();
		session.close();
		return objectSaved;

	}

	public static Object save(Object obj) {
		SessionFactory sf = con.buildSessionFactory();
		Session session = sf.openSession();
		Transaction t = session.beginTransaction();

		Object objSaved = session.save(obj);
		System.out.println(objSaved);

		t.commit();
		session.close();
		return obj;
	}

	public static Object update(Object obj) {
		SessionFactory sf = con.buildSessionFactory();
		Session session = sf.openSession();
		Transaction t = session.beginTransaction();
		session.update(obj);
		t.commit();
		session.close();
		return obj;
	}

	public static void delete(int id, Class clazz) {
		SessionFactory sf = con.buildSessionFactory();
		Session session = sf.openSession();
		Transaction t = session.beginTransaction();
		Object obj = (Object) session.createCriteria(clazz).add(Restrictions.eq("id",id))
				.uniqueResult();
		session.delete(obj);

		/*
		 * Criteria cr=session.createCriteria(clazz); Criterion
		 * cn=Restrictions.eq("id", id);
		 * 
		 * Object obj=(Object)cr.add(cn); session.delete(obj);
		 */
		t.commit();
		session.close();
	}

}
