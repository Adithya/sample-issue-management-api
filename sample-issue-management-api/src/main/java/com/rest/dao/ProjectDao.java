package com.rest.dao;

import java.util.List;
import com.rest.model.Project;

public class ProjectDao extends BaseDao {

	public List<Project> getAll(Class clazz) {
		return (List) super.get(Project.class);
	}

	public Project getById(int id) {
		return (Project) super.getById(id, Project.class);
	}

	public Project create(Project project) {
		return (Project) super.save(project);
	}

	public Project update(Project project){
		return (Project) super.update(project);
	}
	
	public void delete(int id){
		super.delete(id, Project.class);
	}
}
