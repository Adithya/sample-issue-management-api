package com.rest.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.rest.model.Customer;

@Component
public class CustomerDao {

	private static List customers;
	{
		customers = new ArrayList();
		customers.add(new Customer(101, "John", "Doe"));
		customers.add(new Customer(201, "Russ", "Smith"));
		customers.add(new Customer(301, "Kate", "Williams"));
	}

	public List list() {
		return customers;
	}

}
