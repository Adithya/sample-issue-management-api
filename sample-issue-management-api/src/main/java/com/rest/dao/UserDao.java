package com.rest.dao;

import java.util.List;
import com.rest.model.User;

public class UserDao extends BaseDao {

	public List<User> getAll(Class clazz) {
		return (List) super.get(User.class);
	}

	public User getById(int id) {
		return (User) super.getById(id, User.class);
	}

	public User create(User user){
		return(User) super.save(user);
	}

	public User update(User user){
		return (User) super.update(user);
	}
	
	public void delete(int id){
		super.delete(id, User.class);
	}
}
