//This class will replace web.xml and it will map the spring�s dispatcher servlet and bootstrap it.
package com.rest.configuration;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class ApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

	@Override
	protected Class[] getRootConfigClasses() {
		return new Class[] { ApplicationConfiguration.class };
				
	}

	@Override
	protected Class[] getServletConfigClasses() {
		return null;
				
	}

	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}
}
