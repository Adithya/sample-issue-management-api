package com.rest.controller;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.rest.dao.IssueDao;
import com.rest.model.Issue;

@RestController
public class IssueController {

	IssueDao dao = new IssueDao();

	@GetMapping("/issues")
	public List<Issue> getIssues() {
		return (List) dao.getAll(Issue.class);
	}

	@GetMapping("/issues/{id}")
	public Issue getIssue(@PathVariable("id") int id) {
		return (Issue) dao.getById(id);
	}

	@PostMapping("/issue")
	public Issue createIssue(@RequestBody Issue issue){
		return (Issue) dao.create(issue); 
	}
	
	@PutMapping("/issue")
	public Issue updateIssue(@RequestBody Issue issue){
		return (Issue) dao.update(issue);
	}
	
	@DeleteMapping("issues/{id}")
	public void deleteUpadate(@PathVariable int id){
		dao.delete(id);
	}
}
