package com.rest.controller;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.rest.dao.ProjectDao;
import com.rest.model.Project;

@RestController
public class ProjectController {
	private final ProjectDao dao = new ProjectDao();

	@GetMapping("/projects")
	public List<Project> getCustomers() {
		return (List) dao.getAll(Project.class);
	}

	// @PathVariable is used to assign the path specific value.
	@GetMapping("/projects/{id}")
	public Project getCustomer(@PathVariable("id") int id) {
		return (Project) dao.getById(id);
	}

	@PostMapping("/project")
	// @RequestBody id used get the values from the json format
	public Project createProject(@RequestBody Project project) {
		System.out.println(project.toString());
		return (Project) dao.create(project);
	}

	@PutMapping("/project")
	public Project updateProject(@RequestBody Project project){
		return (Project) dao.update(project);
	}
	
	@DeleteMapping("/projects/{id}")
	public void dleteProject(@PathVariable("id") int id ){
		dao.delete(id);
	}
}
