package com.rest.controller;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.rest.dao.UserDao;
import com.rest.model.User;

@RestController
public class UserController {

	UserDao dao = new UserDao();

	@GetMapping("/users")
	public List<User> getUsers() {
		return (List) dao.getAll(User.class);
	}

	@GetMapping("/users/{id}")
	public User getUser(@PathVariable("id") int id) {
		return (User) dao.getById(id);
	}

	@PostMapping("/user")
	public User createUser(@RequestBody User user) {
		return (User) dao.create(user);
	}
	
	@PutMapping("/user")
	public User updateUser(@RequestBody User user){
		return (User) dao.update(user);
	}
	
	@DeleteMapping("/users/{id}")
	public void deleteUser(@PathVariable("id") int id){
		dao.delete(id);
	}
}
